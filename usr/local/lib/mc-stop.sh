#!/bin/sh

# Exit if variable NAME is not set or the PID is 0 (not set)
PID="$(systemctl show --property MainPID --value "mc@${NAME?}")"
if [ 0 = "$PID" ]; then
	echo "No PID! Server is not running?"
	exit 1
fi

# Exit if the named pipe doesn't exist
PIPE="/run/mc-${NAME}"
if [ ! -p "$PIPE" ]; then
	echo "$PIPE doesn't exist or isn't a named pipe."
	exit 1
fi

run() {
	# Send all arguments of this function to the server as a single line
	printf '%s\n' "$*" > "$PIPE"
}

# Source custom stop.sh if it exists and executable
STOP="/srv/$NAME/stop.sh"
if [ -f "$STOP" ] && [ -x "$STOP" ]; then
	# shellcheck disable=1090
	. "$STOP"
else
	# Run Minecraft's regular "stop" command if stop.sh is not provided
	run stop
fi

# Wait for the server to exit (or for systemd to kill us)
while kill -0 "$PID" 2> /dev/null; do
	sleep 1
done
